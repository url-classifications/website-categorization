# Website categorization

## Getting started

This is repository for a machine learning model, capable of website categorization according to both Google products as well as IAB taxonomies. 

The difference between both taxonomies is that whereas Google products taxonomy is suitable for Ecommerce domain (e.g. categorizing products in online stores), IAB is more intended for texts and URLs categorization for marketing purposes. 

A typical example of the latter is real-time-bidding where it is necessary to know categories of potential websites, so that the ads can be placed on those websites that have category same or similar to the one of ad. 

Another common use is to [web content filters by companies who want to implement internal policies on which content is acceptable to be viewed ](https://www.websitecategorizationapi.com). An employer would e.g. want to restrict what websites their employees visit during work hours so that they do not visit shopping sites, game sites, social media networks and so on. 

Further use of Website categorization is in the area of cybersecurity where we would like to find out if given website is malicious or not. 

Example of malicious websites are those that are phishing websites. Phishing attacks can use different methods, some do link manipulation (which is making malicious website look like legitimate domain), others carry our evasion of filters (one example would be using images of websites), then others make a forgery of website (by injecting javascript or some other code at otherwise genuine website), still others use social engineering.

When building a machine learning model for detecting malicious websites there are lot of featues one can choose from, e.g.: 

does URL use IP 
number of dots in URL 
number of days since registration of domain
the length of URL
presence of @ symbol
existence of double slash redirection using //
prefix, suffix in URL. These are used to make the domain look more like legitimate website. 
are any objects (e.g. image, video) being loaded from another domain than the one being used
is HTTP protocol being used
links that are being used in tags
number of redirects

These are just some of them. An interesting article on the topic of feature engineering for this kind of ML model is: [https://www.ml.cmu.edu/research/dap-papers/dap-guang-xiang.pdf](https://www.ml.cmu.edu/research/dap-papers/dap-guang-xiang.pdf). 

But back to website categorization code. 

You can find definitions of both taxonomies that are used in our code here: 

- IAB: https://www.iab.com/guidelines/content-taxonomy/
- Google products taxonomy: https://www.google.com/basepages/producttype/taxonomy-with-ids.en-US.txt

## How the IAB and GPT classifiers were built
 
Website categorization is a supervised machine learning model, so one needs to prepare a training data set which consists of texts that have their category assigned. 

There are multiple approaches to build such training data set, ranging from purchasing them or by building them on your own. The latter can include gathering texts themselves and then classifying them by some method or by using already labelled categories for this purpose. 

The key in classifier development is that the training data set is sufficiently large. This also depends on the number of categories. E.g. Tier 1 classifier with 21 categories may not require as much data items as the Tier 3 classifier with more than 1000+ categories. 

Once the training data set is prepared, next step is select the appropriate machine learning or deep learning model. 

A good practice when developing machine learning model for text classification is to include as much ML models as possible, with some serving as the baseline models, e.g.: 

- naive bayes
- support vector machines
- decision trees
- random forests

An important consideration of which machine learning models concerns inference speed. If you go with classical machine learning models but used on training data set with 1000+ categories you may have a problem when doing predictions or inference. As it may much longer to predict category than if you are using e.g. neural net model instead, e.g. LSTM or CNN. 

An interesting application of website categorization is combining with [IP address search](https://reverseiplookupapi.com/) to then offer the service of displaying all domains of certain Tier n category on specific range of IP domains. 

## OCR API

OCR is a technology that allows for extraction of texts from images, a great library is e.g. [https://guides.nyu.edu/tesseract](https://guides.nyu.edu/tesseract). 
One of the interesting ways to upgrade the categorizer is to combine it with [Optical Character Recognition or online OCR](https://ocrapi.io/) so that the classification is done not only on the text from the website but also on the text extracted from the images of the website. 

## Libraries for text classification

Libraries that we have used in the past for website categorization and broadly text classification models include: 

- [https://www.tensorflow.org/](https://www.tensorflow.org/)
- [https://keras.io/](https://keras.io/)
- [https://scikit-learn.org/stable/index.html](https://scikit-learn.org/stable/index.html)


## Installation
You can use website categorization using NodeJS modules: 

- [https://www.npmjs.com/package/websitecategorization](https://www.npmjs.com/package/websitecategorization)
- [https://npmmirror.com/package/websitecategorization](https://npmmirror.com/package/websitecategorization)
- [https://yarnpkg.com/package/websitecategorization](https://yarnpkg.com/package/websitecategorization)

## Examples of API calls (curl)

```
curl --location --request POST 'https://www.websitecategorizationapi.com/api/gpt/gpt_category1.php' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'query=polaroid land camera' \
--data-urlencode 'api_key=b4dcde2ce5fb2d0b887b5e'

curl --location --request POST 'https://www.websitecategorizationapi.com/api/gpt/gpt_category2.php' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'query=polaroid land camera' \
--data-urlencode 'api_key=b4dcde2ce5fb2d0b887b5e'

curl --location --request POST 'https://www.websitecategorizationapi.com/api/gpt/gpt_category3.php' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'query=polaroid land camera' \
--data-urlencode 'api_key=b4dcde2ce5fb2d0b887b5e'

```

## Example of classifications

Classifying text (product name) of "keyboard wrist rest pad mouse pad" results in the following classification output (Tier 1, Ecommerce classifier): 
```
{
  "classification": [
    {
      "category": "Electronics",
      "value": 0.9763330948695568
    },
    {
      "category": "Sporting Goods",
      "value": 0.012885290547490522
    },
    {
      "category": "Office Supplies",
      "value": 0.003113368701752937
    },
    {
      "category": "Home & Garden",
      "value": 0.0025092741891061124
    },
    {
      "category": "Health & Beauty",
      "value": 0.0012401576273237337
    },
    {
      "category": "Arts & Entertainment",
      "value": 0.0011296330514857802
    },
    {
      "category": "Mature",
      "value": 0.0007678554915459984
    },
    {
      "category": "Hardware",
      "value": 0.0006132685698620763
    },
    {
      "category": "Toys & Games",
      "value": 0.0005193823850344615
    },
    {
      "category": "Software",
      "value": 0.00023125784354711254
    },
    {
      "category": "Furniture",
      "value": 0.0001634231072697243
    },
    {
      "category": "Animals & Pet Supplies",
      "value": 0.00013759378506921096
    },
    {
      "category": "Baby & Toddler",
      "value": 0.00010842523296059142
    },
    {
      "category": "Apparel & Accessories",
      "value": 0.000055634958334119265
    },
    {
      "category": "Media",
      "value": 0.00004585058937549943
    },
    {
      "category": "Vehicles & Parts",
      "value": 0.00004168973093455602
    },
    {
      "category": "Business & Industrial",
      "value": 0.00003341701311768531
    },
    {
      "category": "Religious & Ceremonial",
      "value": 0.00003033609657202394
    },
    {
      "category": "Luggage & Bags",
      "value": 0.000016103284801887052
    },
    {
      "category": "Food, Beverages & Tobacco",
      "value": 0.000014303427320454812
    },
    {
      "category": "Cameras & Optics",
      "value": 0.0000106394975384945
    }
  ],
  "language": "en"
}
```

Classifying website/URL of "www.allure.com" with IAB Classifier results in the following output: 

```
{
  "classification": [
    {
      "category": "Style & Fashion",
      "value": 0.9433921525767678
    },
    {
      "category": "Events and Attractions",
      "value": 0.03402403682776309
    },
    {
      "category": "Pop Culture",
      "value": 0.011849226248542455
    },
    {
      "category": "Books and Literature",
      "value": 0.002645393112836457
    },
    {
      "category": "Shopping",
      "value": 0.002171763481568925
    },
    {
      "category": "Fine Art",
      "value": 0.0012914223259017851
    },
    {
      "category": "Religion & Spirituality",
      "value": 0.000775566172574035
    },
    {
      "category": "Family and Relationships",
      "value": 0.000612831088463565
    },
    {
      "category": "Careers",
      "value": 0.0004630646188382277
    },
    {
      "category": "Travel",
      "value": 0.0004437201027977675
    },
    {
      "category": "Food & Drink",
      "value": 0.00041548241143800075
    },
    {
      "category": "Healthy Living",
      "value": 0.0003599880578840198
    },
    {
      "category": "Television",
      "value": 0.00033747379959067305
    },
    {
      "category": "Medical Health",
      "value": 0.0002673064932040328
    },
    {
      "category": "Hobbies & Interests",
      "value": 0.00026184833793149894
    },
    {
      "category": "Home & Garden",
      "value": 0.00014676104457553933
    },
    {
      "category": "Movies",
      "value": 0.00011627460917520951
    },
    {
      "category": "Pets",
      "value": 0.00010035348998700001
    },
    {
      "category": "Video Gaming",
      "value": 0.00006715431646761172
    },
    {
      "category": "Education",
      "value": 0.0000484771225460187
    },
    {
      "category": "Music and Audio",
      "value": 0.00004575976053896264
    },
    {
      "category": "Sports",
      "value": 0.000045254163105857505
    },
    {
      "category": "Real Estate",
      "value": 0.000028475884625317234
    },
    {
      "category": "Automotive",
      "value": 0.000024973761560101073
    },
    {
      "category": "Personal Finance",
      "value": 0.000018097989897550807
    },
    {
      "category": "News and Politics",
      "value": 0.00001701316645132834
    },
    {
      "category": "Technology & Computing",
      "value": 0.000016470072642019275
    },
    {
      "category": "Science",
      "value": 0.000011201596468945909
    },
    {
      "category": "Business and Finance",
      "value": 0.000002457365856187739
    }
  ],
  "language": "en"
}
```

## Articles on topic of website and product categorization 

If you want to learn more about website categorization and product categorization, we invite you to check out the following articles and resources: 

- [Taxonomies - IAB and Google products](https://medium.com/product-categorization/product-categorization-introduction-d62bb92e8515)
- [machine learning on patreon](https://www.patreon.com/mlcode)
- [https://explainableaixai.github.io/websitesclassification/](https://explainableaixai.github.io/websitesclassification/)


## Project status
This project is being actively developed. 
